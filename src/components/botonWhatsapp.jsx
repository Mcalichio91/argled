import logo from "../assets/WhatsApp_Logo_2.png";
export default function BotonWhatsapp() {
  return (
    <>
      <a style={{ cursor: "pointer" }} href="https://api.whatsapp.com/send?phone=59897225091" target={'_blank'}>
        <img
          src={logo}
          style={{
            maxWidth: "80px",
            position: "fixed",
            bottom: "3.5rem",
            right: "3.5rem",
            filter: 'drop-shadow(9px 6px 13px lightgreen)'
          }}
          alt="Comunicate con nosotros"
        />
      </a>
    </>
  );
}
